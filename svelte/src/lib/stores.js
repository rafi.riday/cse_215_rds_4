import { writable } from "svelte/store";

export const inputWithLabelSectionClass =
    'whitespace-nowrap w-full child:w-full surface-ios-comp rounded-md flex gap-3 items-center justify-center px-2 py-1 col-span-2';
export const inputWithLabelContainerMainClass = "w-fit mr-auto surface-ios-cont rounded-md p-2.5 grid grid-cols-5 gap-2";

// modals
export const ContactModal = writable(false);
export const ErrorModal = writable(false);
export const InfoModal = writable(false);
// modal msg
export const ErrorMsg = writable('An error ocurred');
export const InfoMsg = writable('Process done');
// semester info
export const CurrentSemester = writable(232);
export const Advising = writable(false);
export const NextSemester = writable(241);
// terminal
export const IsTerminalOpen = writable(false);
export const TerminalMsg = writable([]);
export const TerminalLogFunc = (msg) => {
    let d = new Date();
    let dateStr =
        '<span class="font-bold text-purple-500 dark:text-purple-400">' +
        d.getDate() +
        '-' +
        d.getMonth() +
        '-' +
        JSON.stringify(d.getFullYear()).split('').slice(2).join('') +
        ' ' +
        d.getHours() +
        ':' +
        d.getMinutes() +
        ':' +
        d.getSeconds() +
        ' $</span>';

    msg =
        '<div class="px-2 py-1 w-full surface-ios-comp shadow-lg">' +
        dateStr +
        '<br />' +
        msg +
        '</div>';
    TerminalMsg.update(arr => [msg, ...arr]);
};

export const AdminSelectedCourse = writable({
    courseCode: '',
    faculty: '',
    section: null,
    semester: null,
});
