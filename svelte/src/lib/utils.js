export const returnPagePath = (pathname) => {
    let arr = pathname.split('/');
    arr = arr.filter(a => a !== '');
    arr = arr.slice(arr.indexOf('html'));
    if (arr.filter(a => a == 'html').length !== 1)
        while (arr.filter(a => a == 'html').length > 1)
            arr = arr.slice(arr.indexOf('html') + 1);

    return arr;
}

export const allowInputInt = e => e.target.value = e.target.value.replace(/[^0-9]/g, '');
export const allowInputFloat = e => {
    const currentValue = e.target.value;
    const newValue = currentValue.replace(/[^0-9.]/g, '');
    const selectionStart = e.target.selectionStart;

    if (newValue !== currentValue) {
        const cursorPosition = selectionStart >= 0 ? selectionStart : newValue.length;
        e.target.value = newValue;
        e.target.setSelectionRange(cursorPosition, cursorPosition);
    }
};
export const allowInputIntString = e => e.target.value = e.target.value.replace(/[^0-9A-Za-z]/g, '');
export const allowInputCourseTime = e => e.target.value = e.target.value.replace(/[^0-9A-Za-z\s:-]/g, '');
