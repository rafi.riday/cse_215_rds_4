/** @type {import('tailwindcss').Config} */
export default {
    content: ['./src/**/*.{html,js,svelte,ts}', './node_modules/flowbite-svelte/**/*.{html,js,svelte,ts}'],

    plugins: [
        function ({ addVariant }) {
            addVariant('child', '& > *')
        },
        require('flowbite/plugin')],
    darkMode: 'class',

    theme: {
        extend: {
            colors: {
                primary: {
                    50: "#d9e6ff",
                    100: "#cddefe",
                    200: "#c0d6fe",
                    300: "#9abdfe",
                    400: "#4f8cfd",
                    500: "#035bfc",
                    600: "#0352e3",
                    700: "#0244bd",
                    800: "#023797",
                    900: "#012d7b",
                },
                purple: {
                    50: "#faf5ff",
                    100: "#f3e8ff",
                    200: "#e9d5ff",
                    300: "#d8b4fe",
                    400: "#c084fc",
                    500: "#a855f7",
                    600: "#9333ea",
                    700: "#7e22ce",
                    800: "#6b21a8",
                    900: "#581c87"
                },
                surface: {
                    50: "#eeeefa",
                    100: "#dfdfea",
                    200: "#c6c6cf",
                    300: "#a4a4af",
                    400: "#60606f",
                    500: "#1c1c1d",
                    600: "#19191a",
                    700: "#151516",
                    800: "#111112",
                    900: "#0e0e0f",
                },
                "surface-ios": {
                    "comp": "#fff",
                    "cont": "#d7d7e0",
                    "bg": "#efeff4",
                    "gray": "#98989d",
                    "comp-dark": "#2b2b2c",
                    "cont-dark": "#141415",
                    "bg-dark": "#000",
                }
            }
        },
    },
}

/*

*/