module rds {
    // requires javafx.controls;
    requires org.mongodb.driver.sync.client;
    requires org.mongodb.bson;
    requires org.mongodb.driver.core;
    requires javafx.web;
    requires javafx.media;
    requires com.fasterxml.jackson.databind;
    requires jdk.xml.dom;

    opens lib.model.course to com.fasterxml.jackson.databind;

    exports rds;
}