package rds;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import lib.web.Web;

public class App extends Application {
    private static Scene scene;

    public static void main(String[] args) {
        launch(args);
    }

    @SuppressWarnings("exports")
    @Override
    public void start(Stage primaryStage) {
        Web.setEngine(Web.getWebview().getEngine());
        Web.getEngine().load(App.class.getResource("html/index.html").toExternalForm());

        /* ********* */
        Web.getEngine().setOnAlert(event -> Web.onUIAlert(event.getData()));
        Web.getEngine().setConfirmHandler(message -> Web.onUIConfirm(message));
        Web.getEngine().setPromptHandler(event -> Web.onUIPrompt(event.getMessage()));
        /* ********* */

        scene = new Scene(new BorderPane(Web.getWebview()));
        primaryStage.setMinHeight(400);
        primaryStage.setMinWidth(600);
        primaryStage.setHeight(600);
        primaryStage.setWidth(1300);
        primaryStage.setTitle("Rds 4.0");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
