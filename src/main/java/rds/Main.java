package rds;

import lib.db.ClientDB;
import lib.db.NsuData;

public class Main {
    public static void main(String[] args) {
        try {
            ClientDB.startClient();
            NsuData.printInfo();

            App.main(args);
        } catch (Exception e) {
            System.out.println("Failed to run : " + e);
        } finally {
            ClientDB.close();
        }
    }
}
