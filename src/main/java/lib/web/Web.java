package lib.web;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.html.HTMLInputElement;

import com.fasterxml.jackson.databind.ObjectMapper;

import javafx.application.Platform;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.util.Pair;
import lib.db.NsuData;
import lib.model.course.Course;

public final class Web {
    public static ObjectMapper jsonWriter = new ObjectMapper();
    public static Document doc;
    private static WebEngine engine;
    private static WebView webview = new WebView();

    public static Document getDoc() {
        return doc;
    }

    public static void setDoc(Document doc) {
        Web.doc = doc;
    }

    public static WebEngine getEngine() {
        return engine;
    }

    public static void setEngine(WebEngine engine) {
        Web.engine = engine;
    }

    public static void setWebview(WebView _webview) {
        webview = _webview;
    }

    public static WebView getWebview() {
        return webview;
    }

    public static String getInputValueById(Document doc, String id) {
        Element element = doc.getElementById(id);
        if (element instanceof HTMLInputElement) {
            return ((HTMLInputElement) element).getValue();
        }
        return "";
    }

    public static Element removeByID(Document doc, String id) {
        Element elementToRemove = doc.getElementById(id);
        if (elementToRemove != null) {
            Node parentNode = elementToRemove.getParentNode();
            if (parentNode != null && parentNode.getNodeType() == Node.ELEMENT_NODE) {
                Element parentElement = (Element) parentNode;
                parentElement.removeChild(elementToRemove);
                return parentElement;
            }
        }

        return null;
    }

    public static void onUIAlert(String message) {
        if ("$Java StartEngine".equals(message)) {
            doc = engine.getDocument();

            if (doc != null) {
                /**
                 * Examples
                 */
                // Element span_a = doc.getElementById("changeFromJava");
                // span_a.setTextContent("Element accessed from Java");

                // Element div_a = doc.getElementById("parent-a");
                // while (div_a.hasChildNodes()) {
                // div_a.removeChild(div_a.getFirstChild());
                // }

                // Element superParent = Web.removeByID(doc, "parent-a");
                // Element newChild = doc.createElement("div");
                // newChild.setAttribute("style", "background-color: red;");
                // Element btn = doc.createElement("button");
                // btn.setAttribute("class", btn.getAttribute("class") + " btn");
                // btn.setAttribute("id", "lala");
                // newChild.appendChild(btn);
                // superParent.appendChild(newChild);

                // Element btn2 = doc.getElementById("lala");
                // btn2.setTextContent("lala");
            }
        } else if (message.isEmpty() || message == null) {
        } else if ("$Java Exit".equals(message)) {
            Platform.exit();
        } else if (message.startsWith("$Java Print ")) {
            System.out.println(message.replace("$Java Print ", ""));
        } else if (message.startsWith("$Java Zoom")) {
            if (message.endsWith("in"))
                webview.setZoom(webview.getZoom() + 0.1);
            else if (message.endsWith("out"))
                webview.setZoom(webview.getZoom() - 0.1);

        } else {
            Dialog<Void> alert = new Dialog<>();
            alert.getDialogPane().setContentText(message);
            alert.getDialogPane().getButtonTypes().add(ButtonType.OK);
            alert.showAndWait();
        }
    }

    public static boolean onUIConfirm(String message) {
        doc = engine.getDocument();

        Dialog<ButtonType> confirm = new Dialog<>();
        confirm.getDialogPane().setContentText(message);
        confirm.getDialogPane().getButtonTypes().addAll(ButtonType.YES, ButtonType.NO);
        boolean result = confirm.showAndWait().filter(ButtonType.YES::equals).isPresent();

        return result;
    }

    public static String onUIPrompt(String message) {
        doc = engine.getDocument();

        if ("send-json".equals(message)) {
            Course course = new Course("course code", 0, "faculty", "time", "room",
                    0);

            try {
                return jsonWriter.writeValueAsString(course);
            } catch (Exception e) {
                System.out.println(e);
                return "Failed to resolve data";
            }
        } else if ("admin-all-courses-get-info".equals(message)) {
            String findCourse = getInputValueById(doc, "admin-all-courses-input-course");
            int findSection = 0;
            try {
                findSection = Integer.parseInt(getInputValueById(doc, "admin-all-courses-input-section"));
            } catch (Exception e) {
            }
            String findFaculty = getInputValueById(doc, "admin-all-courses-input-faculty");
            int findSemester = 0;
            try {
                findSemester = Integer.parseInt(getInputValueById(doc, "admin-all-courses-input-semester"));
            } catch (Exception e) {
            }
            Pair<Boolean, String> res = NsuData
                    .findAll(new Course(findCourse, findSection, findFaculty, null, null, findSemester), 1);

            return "{\"error\": " + !res.getKey() + ", \"data\": " + res.getValue() + "}";
        }

        return "";
    }
}
