package lib.db;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

import lib.hidden.MongoDBInfo;

public final class ClientDB {
    private static MongoClient client;

    public static final void startClient() {
        if (client == null && MongoDBInfo.getURI() != null) {
            client = MongoClients.create(MongoDBInfo.getURI());
            MongoDBInfo.setURI(null);
        }
    }

    public static final MongoClient getClient() {
        startClient();
        return client;
    }

    public static final void close() {
        if (client != null)
            client.close();
    }
}
