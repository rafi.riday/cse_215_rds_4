package lib.db;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.result.UpdateResult;

import javafx.util.Pair;
import lib.model.course.Course;
import lib.model.course.CourseList;
import lib.web.Web;

public final class NsuData {
    private static MongoCollection<Document> collection = ClientDB.getClient().getDatabase("cse_215_rds_4")
            .getCollection("course-data");

    private static void putStringCmp(Document filter, String field, String value, Boolean isExplicit) {
        if (filter != null && field != null && !field.isEmpty() && value != null && !value.isEmpty()) {
            String pattern = isExplicit ? "^" + value + "$" : ".*" + value + ".*";
            Document regexFilter = new Document("$regex", pattern);
            regexFilter.append("$options", "i");
            filter.put(field, regexFilter);
        }
    }

    private static Document getFilter(Course course, boolean isExplicit) {
        Document filter = new Document();

        if (course.getSemester() > 0)
            filter.put("semester", course.getSemester());

        if (course.getSection() > 0)
            filter.put("section", course.getSection());

        putStringCmp(filter, "courseCode", course.getCourseCode(), isExplicit);
        putStringCmp(filter, "faculty", course.getFaculty(), isExplicit);
        putStringCmp(filter, "room", course.getRoom(), isExplicit);
        putStringCmp(filter, "time", course.getTime(), isExplicit);

        if (filter.isEmpty())
            filter.put("semester", new Document("$gte", 0));
        return filter;
    }

    /**
     * Requires course code and faculty||section
     * returns targeted course or null
     */
    public static final Pair<Boolean, Course> findOne(Course course) {
        if (course.getCourseCode() != null
                && ((course.getFaculty() != null && !course.getFaculty().isEmpty()) || course.getSection() > 0)) {
            Document filter = getFilter(course, true);
            try {
                Document doc = collection.find(filter).first();

                if (doc != null)
                    return new Pair<>(true,
                            new Course(doc.getString("courseCode"), doc.getInteger("section"),
                                    doc.getString("faculty"), doc.getString("time"), doc.getString("room"),
                                    doc.getInteger("semester"),
                                    doc.getList("students", String.class)));
            } catch (Exception e) {
                System.out.println("Error ocurred related to DB (NsuData.findOne) : " + e);
            }
        }
        return new Pair<>(false, null);
    }

    public static final Pair<Boolean, Course> simpleFind(String courseCode, int section,
            String faculty) {
        if (courseCode != null
                && ((faculty != null && !faculty.isEmpty()) || section > 0)) {
            Document filter = getFilter(new Course(courseCode, section, faculty, null, null, 0), true);
            try {
                Document doc = collection.find(filter).first();
                // return collection.find(filter).first();

                if (doc != null)
                    return new Pair<>(true,
                            new Course(doc.getString("courseCode"), doc.getInteger("section"),
                                    doc.getString("faculty"), doc.getString("time"), doc.getString("room"),
                                    doc.getInteger("semester"),
                                    doc.getList("students", String.class)));
            } catch (Exception e) {
                System.out.println("Error ocurred related to DB (NsuData.findOne) : " + e);
            }
        }
        return null;
    }

    /**
     * returns course list, selected page, and total number of pages or null
     */
    public static final Pair<Boolean, String> findAll(Course course, int pageNumber) {
        if (pageNumber < 1)
            pageNumber = 1;
        int skip = (pageNumber - 1) * 50;
        int limit = 50;

        CourseList response = new CourseList();
        try {
            Document filter = getFilter(course, false);

            Bson sort = Sorts.orderBy(Sorts.descending("semester"), Sorts.ascending("courseCode"),
                    Sorts.ascending("section"));

            MongoCursor<Document> cursor = collection.find(filter)
                    .sort(sort)
                    .skip(skip)
                    .limit(limit)
                    .iterator();

            while (cursor.hasNext()) {
                Document doc = cursor.next();

                Course responseCourse = new Course(doc.getString("courseCode"),
                        doc.getInteger("section"),
                        doc.getString("faculty"), doc.getString("time"), doc.getString("room"),
                        doc.getInteger("semester"),
                        doc.getList("students", String.class));

                response.getCourses().add(responseCourse);
            }
            response.setSelectedPage(pageNumber);

            int totalCount = (int) collection.countDocuments(filter);
            response.setTotalPages((int) Math.ceil((double) totalCount / limit));

            if (response.getCourses().size() > 0)
                return new Pair<>(true, Web.jsonWriter.writeValueAsString(response));
        } catch (Exception e) {
            System.out.println("Error ocurred related to DB (NsuData.findAll) : " + e);
        }
        return new Pair<>(false, "Failed to retrieve data");
    }

    public static Pair<Boolean, String> addCourseSection(Course course) {
        try {
            if (course == null)
                return new Pair<>(false, "Invalid course");

            if (course.getCourseCode() == null || course.getCourseCode().isEmpty() ||
                    course.getFaculty() == null || course.getFaculty().isEmpty() ||
                    course.getTime() == null || course.getTime().isEmpty() ||
                    course.getRoom() == null || course.getRoom().isEmpty() ||
                    course.getSemester() <= 0 || course.getSection() <= 0)
                return new Pair<>(false, "Required fields cannot be empty");

            // Document filter = new Document("courseCode", course.getCourseCode())
            // .append("section", course.getSection());
            Document filter = new Document();
            putStringCmp(filter, "courseCode", course.getCourseCode(), true);
            filter.put("semester", course.getSemester());

            if (collection.countDocuments(filter) > 0)
                return new Pair<>(false, "A section for this course already exists, switch to edit mode to edit");

            Document document = new Document("courseCode", course.getCourseCode())
                    .append("section", course.getSection())
                    .append("faculty", course.getFaculty())
                    .append("time", course.getTime())
                    .append("room", course.getRoom())
                    .append("semester", course.getSemester())
                    .append("students", course.getStudents());

            collection.insertOne(document);
            return new Pair<>(true, "Course added successfully");
        } catch (Exception e) {
            return new Pair<>(false, "Failed to add message");
        }
    }

    public static Pair<Boolean, String> editCourseSection(Course course) {
        if (course == null)
            return new Pair<>(false, "Invalid course");

        if (course.getCourseCode() == null || course.getCourseCode().isEmpty() || course.getSection() <= 0)
            return new Pair<>(false, "Required Course Code and Section not provided");

        Document filter = new Document();
        Document update = new Document();

        putStringCmp(filter, "courseCode", course.getCourseCode(), true);
        filter.put("section", course.getSection());

        if (course.getFaculty() != null && !course.getFaculty().isEmpty())
            update.put("faculty", course.getFaculty());
        if (course.getTime() != null && !course.getTime().isEmpty())
            update.put("time", course.getTime());
        if (course.getRoom() != null && !course.getRoom().isEmpty())
            update.put("room", course.getRoom());
        if (course.getSemester() > 0)
            update.put("semester", course.getSemester());
        if (course.getStudents() != null && !course.getStudents().isEmpty())
            update.put("students", course.getStudents());

        if (filter.isEmpty())
            return new Pair<>(false, "At least one field must be provided for filtering");

        UpdateResult result = collection.updateOne(filter, new Document("$set", update));

        if (result.getModifiedCount() > 0)
            return new Pair<>(false, "Course updated successfully");
        else
            return new Pair<>(false, "No course found for the given filter");
    }

    public static final void printInfo() {
        // NsuDataFindAllResponse res = findAll(new Course("Demo", 0,
        // "", "",
        // "", 0), 1);

        // if (res != null) {
        // res.getCourses().forEach(course -> {
        // System.out.println(course.getFaculty() + " " + course.getCourseCode());
        // course.getStudents().forEach(s -> {
        // System.out.println(s);
        // });
        // });
        // }

        // Pair<Boolean, Course> course = findOne(new Course("demoCourse", 69, null,
        // null, null, 0));
        // if (course.getKey() != false && course.getValue() != null) {
        // System.out.println(course.getValue().getCourseCode() + ", " +
        // course.getValue().getFaculty());
        // course.getValue().getStudents().forEach(s -> {
        // System.out.println(s);
        // });
        // } else
        // System.out.println("Boolean : " + course.getKey());
    }
}
