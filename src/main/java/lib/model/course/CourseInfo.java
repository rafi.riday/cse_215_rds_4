package lib.model.course;

import java.util.ArrayList;
import java.util.List;

public class CourseInfo {
    private String courseTitle, courseCode;
    private int credit;
    private List<String> availableToDepartment = new ArrayList<>();
    private List<String> prerequisites = new ArrayList<>();
    private List<String> availableFaculty = new ArrayList<>();

    public CourseInfo(String courseTitle, String courseCode, int credit) {
        this.courseTitle = courseTitle;
        this.courseCode = courseCode;
        this.credit = credit;
    }

    public String getCourseTitle() {
        return courseTitle;
    }

    public void setCourseTitle(String courseTitle) {
        this.courseTitle = courseTitle;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public List<String> getAvailableToDepartment() {
        return availableToDepartment;
    }

    public void setAvailableToDepartment(List<String> availableToDepartment) {
        this.availableToDepartment = availableToDepartment;
    }

    public List<String> getPrerequisites() {
        return prerequisites;
    }

    public void setPrerequisites(List<String> prerequisites) {
        this.prerequisites = prerequisites;
    }

    public List<String> getAvailableFaculty() {
        return availableFaculty;
    }

    public void setAvailableFaculty(List<String> availableFaculty) {
        this.availableFaculty = availableFaculty;
    }

}
