package lib.model.course;

import java.util.ArrayList;
import java.util.List;

public class Course {
    private String courseCode;
    private int section;
    private String faculty;
    private String time;
    private String room;
    private int semester;
    private int seats;
    private List<String> students = new ArrayList<>();

    public Course(String courseCode, int section, String faculty, String time, String room, int semester) {
        this.courseCode = courseCode;
        this.section = section;
        this.faculty = faculty;
        this.time = time;
        this.room = room;
        this.semester = semester;
    }

    public Course(String courseCode, int section, String faculty, String time, String room, int semester,
            List<String> students) {
        this.courseCode = courseCode;
        this.section = section;
        this.faculty = faculty;
        this.time = time;
        this.room = room;
        this.semester = semester;
        this.students = students;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public int getSection() {
        return section;
    }

    public void setSection(int section) {
        this.section = section;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public List<String> getStudents() {
        return students;
    }

    public void setStudents(List<String> students) {
        this.students = students;
    }
}
