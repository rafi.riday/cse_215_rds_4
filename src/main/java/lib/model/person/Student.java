package lib.model.person;

import java.util.HashMap;
import java.util.Map;

public class Student extends Person {
    private String id;
    private String department;
    private double cgpa;
    private int creditCompleted;
    Map<String, Double> history = new HashMap<>();

    public Student(String name, String email, String address, String id, String department, double cgpa,
            int creditCompleted) {
        super(name, email, address, id, department);
        this.id = id;
        this.department = department;
        this.cgpa = cgpa;
        this.creditCompleted = creditCompleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public double getCgpa() {
        return cgpa;
    }

    public void setCgpa(double cgpa) {
        this.cgpa = cgpa;
    }

    public int getCreditCompleted() {
        return creditCompleted;
    }

    public void setCreditCompleted(int creditCompleted) {
        this.creditCompleted = creditCompleted;
    }

    public Map<String, Double> getHistory() {
        return history;
    }

    public void addHistory(String key, Double value) {
        this.history.put(key, value);
    }

    public void setHistory(Map<String, Double> history) {
        this.history = history;
    }
}
