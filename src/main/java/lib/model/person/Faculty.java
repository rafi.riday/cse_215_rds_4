package lib.model.person;

import java.util.ArrayList;
import java.util.List;

public class Faculty extends Person {
    List<String> currentCourses = new ArrayList<>();

    public Faculty(String name, String email, String address, String id, String department) {
        super(name, email, address, id, department);
    }
}
