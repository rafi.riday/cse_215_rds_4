package lib.hidden;

public final class MongoDBInfoDemo {
    private static String URI = ""; // store your URI here and rename class to `MongoDBInfo`

    public static String getURI() {
        return URI;
    }

    public static void setURI(String uRI) {
        URI = uRI;
    }
}
